#
# Cookbook Name:: redis
# Recipe:: default
#
# Copyright 2014, Suzuki-Yuki
#
# All rights reserved - Do Not Redistribute
#
# 本レシピは必ずyum系リポジトリを追加した上で行うこと。

#--------------------------------------------------------------------
# redisのインストール
#--------------------------------------------------------------------
package "redis" do
    action :install
    options "--enablerepo=epel --enablerepo=remi"
end

#--------------------------------------------------------------------
# redisサービスの自動起動設定
#--------------------------------------------------------------------
service "redis" do
    action [:start, :enable]
    supports :status => true, :restart => true, :reload => true
end

#----------------------------
# tclパッケージをインストール
#----------------------------
# package "tcl" do
#     action :install
# end
#
# # 作業用ディレクトリの作成
# directory node['redis']['work_dir'] do
#     action :create
#     not_if "ls -d #{node['redis']['work_dir']}"
# end
#
# # 最新のソースコードを取得
# remote_file node['redis']['work_dir'] + node['redis']['source_file_name'] do
#     source node['redis']['source_url_path'] + node['redis']['source_file_name']
#     not_if "ls #{node['redis']['server_install_path']}"
# end
#
# # ソースコードのアーカイブを展開して make && make test && make install
# bash "install_redis_program" do
#     user "root"
#     cwd node['redis']['work_dir']
#     code <<-EOH
#         tar -zxf #{node['redis']['source_file_name']}
#         cd #{::File.basename(node['redis']['source_file_name'], '.tar.gz')}
#         make
#         make install
#     EOH
#     not_if "ls #{node['redis']['server_install_path']}"
# end
#
# # 起動スクリプト
# template "/etc/init.d/redis" do
#     user "root"
#     source "redis.erb"
#     mode "0755"
# end
#
# # サービスを自動起動する設定
# bash "auto_start_redis_server" do
#     user "root"
#     code <<-EOH
#         sudo chkconfig redis on
#     EOH
# end
#
# # サービスを起動する
# bash "start_redis_server" do
#     user "root"
#     cwd "/usr/local/bin"
#     code <<-EOH
#         #{node['redis']['server_install_path']} &
#     EOH
#     not_if "ps aux | grep \[r\]edis-server"
# end