name             'redis'
maintainer       'TOSLOVE.com'
maintainer_email 'yuppi0412@gmail.com'
license          'All rights reserved'
description      'Installs/Configures redis'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.1.0'
