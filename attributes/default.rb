# 作業用ディレクトリ
default['redis']['work_dir'] = '/usr/local/src/redis/'

# ソースコードの URL
default['redis']['source_version']      = '2.8.17'
default['redis']['source_url_path']     = 'http://download.redis.io/releases/'
default['redis']['source_file_name']    = "redis-#{default['redis']['source_version']}.tar.gz"

# redis-server のインストールパス
default['redis']['server_install_path'] = '/usr/local/bin/redis-server'

# redis-server の起動スクリプト置き場所
default['redis']['auto_start_'] = '/usr/local/bin/redis-server'